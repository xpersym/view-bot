const express = require('express');
const bodyParser = require('body-parser');
const { spawn } = require('child_process');

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.post('/execute', (req, res) => {
    const { command } = req.body;

    if (!command) {
        return res.status(400).send('Command is required');
    }

    const args = command.split(' ');
    const child = spawn('node', ['run.js', ...args], {
        detached: true,
        stdio: 'ignore'
    });

    child.unref();

    res.send('Command executed in the background');
});

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
