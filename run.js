const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const randomUseragent = require('random-useragent');
const fs = require('fs/promises');
const { exec } = require('child_process');
const yargs = require('yargs');

puppeteer.use(StealthPlugin());

const getProxies = async () => (await fs.readFile('proxies.txt', 'utf8')).split('\n').map(line => line.trim()).filter(line => line.length > 0);
const getRandomUserAgent = () => {
  const userAgents = randomUseragent.getAll(ua => ['Chrome', 'Firefox', 'Opera'].includes(ua.browserName));
  return userAgents.length ? userAgents[Math.floor(Math.random() * userAgents.length)].userAgent : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36';
};
const getRandomViewport = () => ({ width: Math.floor(Math.random() * 800) + 800, height: Math.floor(Math.random() * 400) + 600 });
const getRandomTimezone = () => ['America/New_York', 'Europe/London', 'Asia/Tokyo', 'Australia/Sydney'][Math.floor(Math.random() * 4)];
const getRandomLanguage = () => ['en-US', 'en-GB', 'fr-FR', 'de-DE', 'es-ES'][Math.floor(Math.random() * 5)];

const launchPage = async (browser, index, proxy, link, debug) => {
  let page;
  try {
    const [ip, port, username, password] = proxy.split(':');
    const userAgent = getRandomUserAgent();
    const viewport = getRandomViewport();
    const timezone = getRandomTimezone();
    const language = getRandomLanguage();

    page = await browser.newPage();
    await page.setUserAgent(userAgent);
    await page.setViewport(viewport);
    if (username && password) await page.authenticate({ username, password });

    const response = await page.goto(link);
    const status = response.status();
    if (debug) console.log(`Page ${index} status: ${status}`);
    if (status !== 200) throw new Error(`Status ${status}`);

    if (debug) console.log(`Page ${index} title: `, await page.title());
    await new Promise(resolve => setTimeout(resolve, 60000));
    await page.close();
    if (debug) console.log(`Page ${index} closed.`);
  } catch (error) {
    if (debug) console.error(`Error in page ${index}:`, error);
    if (page) await page.close();
    if (error.message.includes('ERR_PROXY_CONNECTION_FAILED')) {
      if (debug) console.log(`Page ${index} proxy connection failed, waiting for 1000ms and restarting with new proxy...`);
      await new Promise(resolve => setTimeout(resolve, 1000));
    }
    const proxies = await getProxies();
    const newProxy = proxies[Math.floor(Math.random() * proxies.length)];
    await launchPage(browser, index, newProxy, link, debug);
  }
};

const launchMultiplePages = async (durationSeconds, concurrentPages, link, debug) => {
  const proxies = await getProxies();
  const startTime = Date.now();
  const endTime = startTime + durationSeconds * 1000;

  const browser = await puppeteer.launch({
    headless: true,
    args: [
      `--no-sandbox`, `--disable-setuid-sandbox`, `--disable-dev-shm-usage`, `--disable-accelerated-2d-canvas`, `--disable-gpu`,
      `--autoplay-policy=no-user-gesture-required`, `--enable-features=MediaCapabilities,MediaPlaybackQuality`,
      `--enable-blink-features=HTMLVideoElement,HTMLMediaElement`, `--mute-audio`
    ]
  });

  const launchAndReplacePage = async (index) => {
    while (Date.now() < endTime) {
      const proxy = proxies[Math.floor(Math.random() * proxies.length)];
      await launchPage(browser, index, proxy, link, debug);
    }
  };

  await Promise.all(Array.from({ length: concurrentPages }, (_, i) => launchAndReplacePage(i + 1)));

  await browser.close();
};

const killAllChrome = () => {
  exec('pkill -f chrome', (err, stdout, stderr) => {
    if (err) return console.error(`Error killing Chrome processes: ${err}`);
    console.log(`Chrome processes killed: ${stdout}`);
    if (stderr) console.error(`stderr: ${stderr}`);
  });
};

process.on('SIGINT', () => { console.log('Received SIGINT. Exiting gracefully...'); killAllChrome(); process.exit(); });
process.on('SIGTERM', () => { console.log('Received SIGTERM. Exiting gracefully...'); killAllChrome(); process.exit(); });

const argv = yargs(process.argv.slice(2)).options({
  duration: { type: 'number', demandOption: true },
  concurrent: { type: 'number', demandOption: true },
  link: { type: 'string', demandOption: true },
  debug: { type: 'boolean', default: false }
}).argv;

launchMultiplePages(argv.duration, argv.concurrent, argv.link, argv.debug).catch(console.error);
