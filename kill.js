import { exec } from 'child_process';
import  os from 'os';

function killChrome() {
  const platform = os.platform();

  let command;

  switch (platform) {
    case 'win32':
      command = 'taskkill /F /IM chrome.exe';
      break;
    case 'darwin':
      command = 'killall -9 "Google Chrome"';
      break;
    case 'linux':
      command = 'pkill -9 chrome';
      break;
    default:
      console.error('Unsupported platform:', platform);
      return;
  }

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing command: ${error.message}`);
      return;
    }
    if (stderr) {
      console.error(`Error: ${stderr}`);
      return;
    }
    console.log('Chrome windows killed successfully');
  });
}

killChrome();
